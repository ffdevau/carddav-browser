var cdbRunner = {
  cdbURLpath: null,

  onLoad: function() {
    cdbRunner.cdbURLpath = null;
    var elementRef = document.getElementById("URLconnect");
    elementRef.addEventListener("click", function() { cdbRunner.cdbURLconnect(); }, false);
    elementRef = document.getElementById("URLdisconnect");
    elementRef.addEventListener("click", function() { cdbRunner.cdbURLdisconnect(); }, false);
    elementRef = document.getElementById("contactsbody");
    elementRef.addEventListener("click", function(event) { cdbRunner.cdbContactSelect(event); }, false);
    elementRef = document.getElementById("View-vCard");
    elementRef.addEventListener("click", function(event) { cdbRunner.cdbContactDisplayStage1(); }, false);
  },

  cdbContactSelect: function(clickEvent) {
    if (clickEvent.target.nodeName == 'TD') {
      var elementRef = document.getElementById(clickEvent.target.parentNode.id);
      var itemHREF = elementRef.getAttribute("vcard-href")
      elementRef = document.getElementById(clickEvent.target.parentNode.id+"td");
      document.getElementById("vcardname").setAttribute("vcard-href", itemHREF);
      document.getElementById("vcardname").innerText = elementRef.innerHTML;
      document.getElementById("vcarddiv").hidden = false;
    }
  },

  cdbURLdisconnect: function() {
    cdbRunner.cdbChildNodeCleanup(document.getElementById("contactsbody"));
    var elementRef = document.getElementById("URLinput"); elementRef.disabled = false;
    elementRef = document.getElementById("URLconnect"); elementRef.disabled = false;
    elementRef = document.getElementById("URLdisconnect"); elementRef.disabled = true;
    document.getElementById("vcardname").innerText = "";
    document.getElementById("vcardname").setAttribute("vcard-href", "");
    document.getElementById("vcarddiv").hidden = true;
    document.getElementById("cdbstatus").innerText = "";
  },

  cdbChildNodeCleanup: function(localParentNode) {
    while (localParentNode.firstChild) {
      if (localParentNode.firstChild.hasChildNodes()) {
        cdbRunner.cdbChildNodeCleanup(localParentNode.firstChild);
      }
      localParentNode.removeChild(localParentNode.firstChild);
    }
  },

  UnescapeChars: function(textLine) {
    var localTextLine = String(textLine);
    var localCharOffset = 0;
    var localCharPos = localTextLine.indexOf(String.fromCharCode(92),localCharOffset);
    var localSubString1 = "";
    var localSubString2 = "";
    while (localCharPos >= 0) {
      localSubString1 = localTextLine.substring(0,localCharPos);
      localSubString2 = localTextLine.substring((localCharPos+2),undefined);
      switch (localTextLine.charAt((localCharPos+1))) {
        case ",": localTextLine = String(localSubString1).concat(",",localSubString2);
        break;
        case ";": localTextLine = String(localSubString1).concat(";",localSubString2);
        break;
        case String.fromCharCode(92): localTextLine = String(localSubString1).concat(String.fromCharCode(92),localSubString2);
        break;
        case "n":
        case "N":
          localTextLine = String(localSubString1).concat("\n",localSubString2);
        break;
      }
      localCharOffset = localCharPos + 1;
      localCharPos = localTextLine.indexOf(String.fromCharCode(92),localCharOffset);
    }
    return localTextLine;
  },

  EnforceLineEndgings: function(textLine) {
    var localText = String(textLine);
    var tempCode = String.fromCharCode(11);
    var localCode = String.fromCharCode(13);
    while (localText.indexOf("&#13;") >= 0) {localText = localText.replace("&#13;",localCode);}
    localCode = String.fromCharCode(10);
    while (localText.indexOf("&#10;") >= 0) {localText = localText.replace("&#10;",localCode);}
    localCode = String.fromCharCode(13, 10);
    while (localText.indexOf(localCode) >= 0) {localText = localText.replace(localCode,tempCode);}
    localCode = String.fromCharCode(13);
    while (localText.indexOf(localCode) >= 0) {localText = localText.replace(localCode,tempCode);}
    localCode = String.fromCharCode(10);
    while (localText.indexOf(localCode) >= 0) {localText = localText.replace(localCode,tempCode);}
    localCode = String.fromCharCode(13, 10);
    while (localText.indexOf(tempCode) >= 0) {localText = localText.replace(tempCode,localCode);}
    return localText;
  },

  Unfold: function(textLine) {
    var localText = String(textLine);
    var localEOL = String.fromCharCode(13, 10, 32); // CR+LF+SP
    while (localText.indexOf(localEOL) >= 0) { localText = localText.replace(localEOL,""); }
    localEOL = String.fromCharCode(13, 10, 9); // CR+LF+TAB
    while (localText.indexOf(localEOL) >= 0) { localText = localText.replace(localEOL,""); }
    return localText;
  },

  LineFolding: function(textValue) {
    var lineToFold = String(textValue);
    var foldingEOL = String.fromCharCode(13, 10, 32); // CR+LF+SP
    if (lineToFold.length > 73) {
      lineToFold = lineToFold.substring(0,72) + foldingEOL + cdbRunner.LineFolding(lineToFold.substring(72,undefined));
    }
    return lineToFold;
  },

  LineAppend: function(textValue, appendValue) {
    var tempText = String(textValue);
    var tempText1 = String(appendValue);
    if (tempText.length > 0) {
      if (tempText1.length > 0) { tempText = tempText + String.fromCharCode(13, 10) + tempText1; }
    } else { if (tempText1.length > 0) { tempText = tempText1; } }
    return tempText;
  },

  LineSplit: function(cardItem) {
    var localCardItem = String(cardItem);
    var returnItem = {tag:"", params:"", value:""};
    var localSplitIdx = localCardItem.indexOf(":");
    var tempText = localCardItem.substring(0,localSplitIdx);
    returnItem.value = localCardItem.substring((localSplitIdx+1),undefined);
    localSplitIdx = tempText.indexOf(";");
    if (localSplitIdx > 0) {
      returnItem.tag = tempText.substring(0,localSplitIdx);
      returnItem.params = tempText.substring((localSplitIdx+1),undefined);
    } else {
      returnItem.tag = tempText;
      returnItem.params = "";
    }
    return returnItem;
  },

  UpperCaseInitialLetter: function(textLine) {
    var tempText = String(textLine).toLowerCase();
    var tempArray = tempText.split(",");
    var tempIdx = 0;
    for (tempIdx = 0; tempIdx < tempArray.length; tempIdx++) {
      tempText = String(tempArray[tempIdx]).substring(0,1).toUpperCase();
      tempText = tempText + String(tempArray[tempIdx]).substring(1,undefined);
      tempArray[tempIdx] = tempText;
    }
    tempText = tempArray.join(",");
    return tempText;
  },

  GeoObject: function(geoItem) {
    var localGeoItem = String(geoItem);
    var localGeoObject = {latdeg:"0", latmin:"0", latsec:"0.00", londeg:"0", lonmin:"0", lonsec:"0.00"};
    if (localGeoItem.length > 0) {
      var tempFloat = 0.0;
      var tempDeg = 0.0;
      var tempMin = 0.0;
      var tempSec = 0.0;
      var negValue = false;
      var localGeoArray = localGeoItem.split(";");
      if (localGeoArray.length > 0) {
        // Latitude Conversion
        tempFloat = parseFloat(localGeoArray[0]);
        if (tempFloat < 0) { negValue = true; tempFloat = -1.0 * tempFloat; }
        if (isFinite(tempFloat) == true) {
          tempDeg = parseInt(tempFloat);
          tempFloat = 60.0*(tempFloat - tempDeg);
          tempMin = parseInt(tempFloat);
          tempSec  = 6000.0*(tempFloat - tempMin);
          tempSec = parseInt(tempSec);
          tempSec = parseFloat(tempSec) / 100.0;
          if (negValue == false) { localGeoObject.latdeg = String(tempDeg);
          } else { localGeoObject.latdeg = "-"+String(tempDeg); }
          localGeoObject.latmin = String(tempMin);
          localGeoObject.latsec = String(tempSec);
        }
      }
      if (localGeoArray.length > 1) {
        // Longitude conversion
        tempFloat = parseFloat(localGeoArray[1]);
        if (tempFloat < 0) { negValue = true; tempFloat = -1.0 * tempFloat; } else { negValue = true; }
        if (isFinite(tempFloat) == true) {
          tempDeg = parseInt(tempFloat);
          tempFloat = 60.0*(tempFloat - tempDeg);
          tempMin  = parseInt(tempFloat);
          tempSec  = 6000.0*(tempFloat - tempMin);
          tempSec = parseInt(tempSec);
          tempSec = parseFloat(tempSec) / 100.0;
          if (negValue == false) { localGeoObject.londeg = String(tempDeg);
            } else { localGeoObject.londeg = "-"+String(tempDeg); }
          localGeoObject.lonmin = String(tempMin);
          localGeoObject.lonsec = String(tempSec);
        }
      }
    }
    return localGeoObject;
  },

  SafeSplitOnChar: function(textLine, textChar) {
    var localItemArray = new Array();
    var localEscapedArray = new Array();
    var localUNescapedArray = new Array();
    var searchChar = String.fromCharCode(92) + textChar;
    var localDelim1 = 0;
    var localDelim2 = textLine.indexOf(searchChar,localDelim1);
    while (localDelim2 >= 0) { // Find any escaped versions of the character
      localEscapedArray.push((localDelim2+1));
      localDelim1 = localDelim2 + 2;
      localDelim2 = textLine.indexOf(searchChar,localDelim1);
    }
    searchChar = textChar;
    localDelim1 = 0;
    localDelim2 = textLine.indexOf(searchChar,localDelim1);
    while (localDelim2 >= 0) { // Find any UNescaped versions of the character
      if (localEscapedArray.indexOf(localDelim2) == -1) { localUNescapedArray.push(localDelim2); }
      localDelim1 = localDelim2 + 1;
      localDelim2 = textLine.indexOf(searchChar,localDelim1);
    }
    if (localUNescapedArray.length > 0) { // Now split the line up
      localDelim1 = 0;
      for (localDelim2 = 0; localDelim2 < localUNescapedArray.length; localDelim2++) {
        localItemArray.push(cdbRunner.UnescapeChars(textLine.substring(localDelim1,localUNescapedArray[localDelim2])));
        localDelim1 = localUNescapedArray[localDelim2] + 1;
      }
      if (localDelim1 < textLine.length) { // Tack on the last part of the line if it is missing
        localItemArray.push(cdbRunner.UnescapeChars(textLine.substring(localDelim1,undefined)));
      }
    }
    return localItemArray;
  },

  ParamCheck: function(currParams, defaultParams) {
    var updatedParams = "";
    var tempParams = String(currParams).toLowerCase();
    while (tempParams.indexOf("type=") >= 0) { tempParams = tempParams.replace("type=",""); }
    if (tempParams.length > 0) {
      var definedPref = false;
      if (tempParams.indexOf("pref") >= 0) { definedPref = true; }
      var paramArray = tempParams.split(",");
      var tempIdx = 0;
      if (definedPref == true) {
        tempIdx = paramArray.indexOf("pref");
        if (tempIdx >= 0) { paramArray.splice(tempIdx,1); }
      }
      if (paramArray.length > 0) {
        var tempText = "";
        for (tempIdx = 0; tempIdx < paramArray.length; tempIdx++) {
          tempText = String(paramArray[tempIdx]).substring(0,1).toUpperCase();
          tempText = tempText + String(paramArray[tempIdx]).substring(1,undefined);
          paramArray[tempIdx] = tempText;
        }
        updatedParams = paramArray.join(",");
      } else { updatedParams = String(defaultParams); }
      if (definedPref == true) { if (updatedParams.length > 0) {
        updatedParams = updatedParams + ",Pref";
      } else { updatedParams = "Pref"; } }
    } else { updatedParams = String(defaultParams); }
    return updatedParams;
  },

  cdbTimeOut: function() {
    cdbRunner.cdbURLdisconnect();
    document.getElementById("cdbstatus").innerText = browser.i18n.getMessage("cdbURLtimeout");
  },

  cdbNetProgress: function(channelData) {
    if (channelData.status == 401) {
      channelData.abort();
      document.getElementById("cdbstatus").innerText = browser.i18n.getMessage("cdbURLauthFail");
    }
  },

  cdbSecurityInformation: function(requestChannel, requestURI) {
    document.getElementById("cdbstatus").innerText = browser.i18n.getMessage("NetworkError");
  },

  CleanUpCardDetails: function() {
    // FN
    document.getElementById("vcardFN").innerHTML = "";
    // N
    document.getElementById("vcardNfamily").innerHTML = "";
    document.getElementById("vcardNgiven").innerHTML = "";
    document.getElementById("vcardNmiddle").innerHTML = "";
    document.getElementById("vcardNhprefix").innerHTML = "";
    document.getElementById("vcardNhsuffix").innerHTML = "";
    // NICKNAME
    document.getElementById("divNICKNAME").hidden = true;
    document.getElementById("vcardNICKNAME").innerHTML = "";
    // BDAY
    document.getElementById("divBDAY").hidden = true;
    document.getElementById("vcardBDAYday").innerHTML = "";
    document.getElementById("vcardBDAYmonth").innerHTML = "";
    document.getElementById("vcardBDAYyear").innerHTML = "";
    // URL
    document.getElementById("divURL").hidden = true;
    document.getElementById("vcardURL").innerHTML = "";
    // EMAIL
    document.getElementById("divEMAIL").hidden = true;
    cdbRunner.cdbChildNodeCleanup(document.getElementById("vcardEMAIL"));
    // TEL
    document.getElementById("divTEL").hidden = true;
    cdbRunner.cdbChildNodeCleanup(document.getElementById("vcardTEL"));
    // ADR
    document.getElementById("divADR").hidden = true;
    var itemArray = ['home', 'work', 'parcel', 'postal', 'dom', 'intl'];
    var itemIdx = 0;
    for (itemIdx = 0; itemIdx < itemArray.length; itemIdx++) {
      document.getElementById("divADR"+itemArray[itemIdx]).hidden = true;
      document.getElementById("vcardADR"+itemArray[itemIdx]+"pobox").innerHTML = "";
      document.getElementById("vcardADR"+itemArray[itemIdx]+"other").innerHTML = "";
      document.getElementById("vcardADR"+itemArray[itemIdx]+"street").innerHTML = "";
      document.getElementById("vcardADR"+itemArray[itemIdx]+"city").innerHTML = "";
      document.getElementById("vcardADR"+itemArray[itemIdx]+"state").innerHTML = "";
      document.getElementById("vcardADR"+itemArray[itemIdx]+"postcode").innerHTML = "";
      document.getElementById("vcardADR"+itemArray[itemIdx]+"country").innerHTML = "";
    }
    // LABEL
    document.getElementById("divLABEL").hidden = true;
    itemArray = ['home', 'work', 'parcel', 'postal', 'dom', 'intl'];
    for (itemIdx = 0; itemIdx < itemArray.length; itemIdx++) {
      document.getElementById("divLABEL"+itemArray[itemIdx]).hidden = true;
      document.getElementById("vcardLBL"+itemArray[itemIdx]).innerHTML = "";
    }
    // TITLE
    document.getElementById("divTITLE").hidden = true;
    document.getElementById("vcardTITLE").innerHTML = "";
    // ROLE
    document.getElementById("divROLE").hidden = true;
    document.getElementById("vcardROLE").innerHTML = "";
    // ORG
    document.getElementById("divORG").hidden = true;
    document.getElementById("vcardORG").innerHTML = "";
    // CATEGORIES
    document.getElementById("divCATEGORIES").hidden = true;
    document.getElementById("vcardCATEGORIES").innerHTML = "";
    // CLASS
    document.getElementById("divCLASS").hidden = true;
    document.getElementById("vcardCLASS").innerHTML = "";
    // TZ
    document.getElementById("divTZ").hidden = true;
    document.getElementById("vcardTZ").innerHTML = "";
    // GEO
    document.getElementById("divGEO").hidden = true;
    document.getElementById("vcardGEOlatd").innerHTML = "";
    document.getElementById("vcardGEOlatm").innerHTML = "";
    document.getElementById("vcardGEOlats").innerHTML = "";
    document.getElementById("vcardGEOlongd").innerHTML = "";
    document.getElementById("vcardGEOlongm").innerHTML = "";
    document.getElementById("vcardGEOlongs").innerHTML = "";
    // NOTES
    document.getElementById("divNOTES").hidden = true;
    document.getElementById("vcardNOTES").innerHTML = "";
    // Other vCard Tags
    document.getElementById("divOTHER").hidden = true;
    document.getElementById("vcardOTHER").innerHTML = "";
  },

  AddressDisplay: function(addrType, addrText) {
    var tempText = String(addrText);
    document.getElementById("divADR"+addrType).hidden = false;
    if (tempText.length > 0) {
      var tempArray1 = cdbRunner.SafeSplitOnChar(tempText,";");
      while (tempArray1.length < 7) { tempArray1.push(""); }
      document.getElementById("vcardADR"+addrType+"pobox").innerText = cdbRunner.UnescapeChars(tempArray1[0]);
      document.getElementById("vcardADR"+addrType+"other").innerText = cdbRunner.UnescapeChars(tempArray1[1]);
      document.getElementById("vcardADR"+addrType+"street").innerText = cdbRunner.UnescapeChars(tempArray1[2]);
      document.getElementById("vcardADR"+addrType+"city").innerText = cdbRunner.UnescapeChars(tempArray1[3]);
      document.getElementById("vcardADR"+addrType+"state").innerText = cdbRunner.UnescapeChars(tempArray1[4]);
      document.getElementById("vcardADR"+addrType+"postcode").innerText = cdbRunner.UnescapeChars(tempArray1[5]);
      document.getElementById("vcardADR"+addrType+"country").innerText = cdbRunner.UnescapeChars(tempArray1[6]);
    } else {
      document.getElementById("vcardADR"+addrType+"pobox").innerText = "";
      document.getElementById("vcardADR"+addrType+"other").innerText = "";
      document.getElementById("vcardADR"+addrType+"street").innerText = "";
      document.getElementById("vcardADR"+addrType+"city").innerText = "";
      document.getElementById("vcardADR"+addrType+"state").innerText = "";
      document.getElementById("vcardADR"+addrType+"postcode").innerText = "";
      document.getElementById("vcardADR"+addrType+"country").innerText = "";
    }
  },

  cdbContactDisplayStage3: function(vCardData) {
    // Process the vCard and display the data.
    var localvCardText = String(vCardData);
    cdbRunner.CleanUpCardDetails();
    document.getElementById("vcarddetails").hidden = false;
    var localCardObject = {n:false, fn:false, nickname:false, bday:false,
                           url:false, title:false, role:false, org:false,
                           categories:false, class:false, tz:false, geo:false,
                           note:false, photo:false, logo:false};
    var adrObject = {dom:false, intl:false, postal:false, parcel:false, home:false, work:false};
    var lblObject = {dom:false, intl:false, postal:false, parcel:false, home:false, work:false};
    var emailArray = new Array();
    var telArray = new Array();
    var vCardExtras = "";
    localvCardText = cdbRunner.Unfold(localvCardText);
    var localvCardArray = localvCardText.split(String.fromCharCode(13, 10));
    var localArrayIdx = 0;
    var cardObject = null;
    var cardTag = "";
    var localSplitIdx = 0;
    var tempText = "";
    var tempText1 = "";
    var tempText2 = "";
    var tempText3 = "";
    var tempArray1 = null;
    var tempObject = null;
    for (localArrayIdx = 0; localArrayIdx < localvCardArray.length; localArrayIdx++) {
      tempText1 = String(localvCardArray[localArrayIdx]); tempText1 = tempText1.trim();
      if (tempText1.length > 0) {
        tempText = cdbRunner.LineFolding(tempText1);
        cardObject = cdbRunner.LineSplit(tempText1); // returnItem = {tag:"", params:"", value:""};
        cardTag = cardObject.tag;
        localSplitIdx = cardTag.indexOf(".");
        if (localSplitIdx >= 0) {
          cardTag = cardTag.substring((localSplitIdx+1),undefined).toUpperCase();
        } else { cardTag = cardTag.toUpperCase(); }
        switch (cardTag) {
          case "FN": if (localCardObject.fn == false) {
              localCardObject.fn = true;
              tempText2 = cdbRunner.UnescapeChars(cardObject.value);
              document.getElementById("vcardFN").innerText = tempText2;
            } else { vCardExtras = cdbRunner.LineAppend(vCardExtras, tempText); }
          break;
          case "N": if (localCardObject.n == false) {
              localCardObject.n = true;
              tempArray1 = cdbRunner.SafeSplitOnChar(cardObject.value, ";");
              while (tempArray1.length < 5) { tempArray1.push(""); }
              tempText2 = cdbRunner.UnescapeChars(tempArray1[0]);
              document.getElementById("vcardNfamily").innerText = tempText2;
              tempText2 = cdbRunner.UnescapeChars(tempArray1[1]);
              document.getElementById("vcardNgiven").innerText = tempText2
              tempText2 = cdbRunner.UnescapeChars(tempArray1[2]);
              document.getElementById("vcardNmiddle").innerText = tempText2;
              tempText2 = cdbRunner.UnescapeChars(tempArray1[3]);
              document.getElementById("vcardNhprefix").innerText = tempText2
              tempText2 = cdbRunner.UnescapeChars(tempArray1[4]);
              document.getElementById("vcardNhsuffix").innerText = tempText2;
            } else { vCardExtras = cdbRunner.LineAppend(vCardExtras, tempText); }
          break;
          case "NICKNAME": if (localCardObject.nickname == false) {
              localCardObject.nickname = true;
              tempText2 = cdbRunner.UnescapeChars(cardObject.value);
              document.getElementById("divNICKNAME").hidden = false;
              document.getElementById("vcardNICKNAME").innerText = tempText2;
            } else { vCardExtras = cdbRunner.LineAppend(vCardExtras, tempText); }
          break;
          case "BDAY": if (localCardObject.bday == false) {
              localCardObject.bday = true;
              document.getElementById("divBDAY").hidden = false;
              tempText2 = String(cardObject.value).toUpperCase();
              localSplitIdx = tempText2.indexOf("T");
              if (localSplitIdx > 0) { tempText2 = tempText2.substring(0,localSplitIdx); } // Drop the time component
              localSplitIdx = tempText2.indexOf("-");
              if (localSplitIdx >= 0) {
                document.getElementById("vcardBDAYday").innerText = tempText2.substring(8,undefined);
                document.getElementById("vcardBDAYmonth").innerText = tempText2.substring(5,7);
                document.getElementById("vcardBDAYyear").innerText = tempText2.substring(0,4);
              } else {
                document.getElementById("vcardBDAYday").innerText = tempText2.substring(6,undefined);
                document.getElementById("vcardBDAYmonth").innerText = tempText2.substring(4,6);
                document.getElementById("vcardBDAYyear").innerText = tempText2.substring(0,4);
              }
            } else { vCardExtras = cdbRunner.LineAppend(vCardExtras, tempText); }
          break;
          case "URL": if (localCardObject.url == false) {
              localCardObject.url = true;
              document.getElementById("divURL").hidden = false;
              tempText2 = cdbRunner.UnescapeChars(cardObject.value);
              tempObject = document.createElement("a");
              tempObject.setAttribute("href",tempText2);
              tempObject.innerText = tempText2;
              document.getElementById("vcardURL").appendChild(tempObject);
            } else { vCardExtras = cdbRunner.LineAppend(vCardExtras, tempText); }
          break;
          case "TITLE": if (localCardObject.title == false) {
              localCardObject.title = true;
              tempText2 = cdbRunner.UnescapeChars(cardObject.value);
              document.getElementById("divTITLE").hidden = false;
              document.getElementById("vcardTITLE").innerText = tempText2;
            } else { vCardExtras = cdbRunner.LineAppend(vCardExtras, tempText); }
          break;
          case "ROLE": if (localCardObject.role == false) {
              localCardObject.role = true;
              tempText2 = cdbRunner.UnescapeChars(cardObject.value);
              document.getElementById("divROLE").hidden = false;
              document.getElementById("vcardROLE").innerText = tempText2;
            } else { vCardExtras = cdbRunner.LineAppend(vCardExtras, tempText); }
          break;
          case "ORG": if (localCardObject.org == false) {
              localCardObject.org = true;
              tempText2 = cdbRunner.UnescapeChars(cardObject.value);
              document.getElementById("divORG").hidden = false;
              document.getElementById("vcardORG").innerText = tempText2;
            } else { vCardExtras = cdbRunner.LineAppend(vCardExtras, tempText); }
          break;
          case "CATEGORIES": if (localCardObject.categories == false) {
              localCardObject.categories = true;
              tempText2 = cdbRunner.UnescapeChars(cardObject.value);
              document.getElementById("divCATEGORIES").hidden = false;
              document.getElementById("vcardCATEGORIES").innerText = tempText2;
            } else { vCardExtras = cdbRunner.LineAppend(vCardExtras, tempText); }
          break;
          case "CLASS": if (localCardObject.class == false) {
              localCardObject.class = true;
              tempText2 = cdbRunner.UnescapeChars(cardObject.value);
              document.getElementById("divCLASS").hidden = false;
              document.getElementById("vcardCLASS").innerText = tempText2;
            } else { vCardExtras = cdbRunner.LineAppend(vCardExtras, tempText); }
          break;
          case "TZ": if (localCardObject.tz == false) {
              localCardObject.tz = true;
              tempText2 = cdbRunner.UnescapeChars(cardObject.value);
              document.getElementById("divTZ").hidden = false;
              document.getElementById("vcardTZ").innerText = tempText2;
            } else { vCardExtras = cdbRunner.LineAppend(vCardExtras, tempText); }
          break;
          case "GEO": if (localCardObject.geo == false) {
              localCardObject.geo = true;
              tempObject = cdbRunner.GeoObject(cardObject.value);
              document.getElementById("divGEO").hidden = false;
              document.getElementById("vcardGEOlatd").innerText = tempObject.latdeg;
              document.getElementById("vcardGEOlatm").innerText = tempObject.latmin;
              document.getElementById("vcardGEOlats").innerText = tempObject.latsec;
              document.getElementById("vcardGEOlongd").innerText = tempObject.londeg;
              document.getElementById("vcardGEOlongm").innerText = tempObject.lonmin;
              document.getElementById("vcardGEOlongs").innerText = tempObject.lonsec;
            } else { vCardExtras = cdbRunner.LineAppend(vCardExtras, tempText); }
          break;
          case "NOTE": if (localCardObject.note == false) {
              localCardObject.note = true;
              tempText2 = cdbRunner.UnescapeChars(cardObject.value);
              document.getElementById("divNOTES").hidden = false;
              document.getElementById("vcardNOTES").innerText = tempText2;
            } else { vCardExtras = cdbRunner.LineAppend(vCardExtras, tempText); }
          break;
          case "EMAIL": tempText2 = String(cardObject.params).toUpperCase();
            tempText2 = cdbRunner.ParamCheck(tempText2,"Internet") + ":" + cardObject.value;
            emailArray.push(tempText2);
          break;
          case "TEL": tempText2 = String(cardObject.params).toLowerCase();
            while (tempText2.indexOf("type=") >= 0) { tempText2 = tempText2.replace("type=",""); }
            tempArray1 = tempText2.split(",");
            localSplitIdx = tempArray1.indexOf("pref");
            if (localSplitIdx >= 0) {
              tempArray1.splice(localSplitIdx,1);
              tempText2 = "Pref";
            } else { tempText2 = ""; }
            if (tempArray1.length == 0) { tempArray1.push("Voice"); }
            for (localSplitIdx = 0; localSplitIdx < tempArray1.length; localSplitIdx++) {
              tempText3 = tempArray1[localSplitIdx];
              if (tempText3.length > 0) {
                if (tempText2.length > 0) {
                  tempText3 = tempText3 + "," + tempText2 + ":" + cardObject.value;
                } else { tempText3 = tempText3 + ":" + cardObject.value; }
                telArray.push(tempText3);
              }
            }
          break;
          case "ADR": tempText2 = String(cardObject.params).toLowerCase();
            document.getElementById("divADR").hidden = false;
            while (tempText2.indexOf("type=") >= 0) { tempText2 = tempText2.replace("type=",""); }
            tempArray1 = tempText2.split(",");
            localSplitIdx = tempArray1.indexOf("pref");
            if (localSplitIdx >= 0) { tempArray1.splice(localSplitIdx,1); }
            for (localSplitIdx = 0; localSplitIdx < tempArray1.length; localSplitIdx++) {
              tempText3 = tempArray1[localSplitIdx];
              if (tempText3.indexOf("language=") >= 0) { tempArray1.splice(localSplitIdx,1); }
            }
            if (tempArray1.length == 0) {
              tempArray1.push("intl");
              tempArray1.push("postal");
              tempArray1.push("parcel");
              tempArray1.push("work");
            }
            for (localSplitIdx = 0; localSplitIdx < tempArray1.length; localSplitIdx++) {
              switch (tempArray1[localSplitIdx]) {
                case "dom": if (adrObject.dom == false) {
                    adrObject.dom = true;
                    cdbRunner.AddressDisplay("dom", cardObject.value);
                  } else { vCardExtras = cdbRunner.LineAppend(vCardExtras, tempText); }
                break;
                case "intl": if (adrObject.intl == false) {
                    adrObject.intl = true;
                    cdbRunner.AddressDisplay("intl", cardObject.value);
                  } else { vCardExtras = cdbRunner.LineAppend(vCardExtras, tempText); }
                break;
                case "postal": if (adrObject.postal == false) {
                    adrObject.postal = true;
                    cdbRunner.AddressDisplay("postal", cardObject.value);
                  } else { vCardExtras = cdbRunner.LineAppend(vCardExtras, tempText); }
                break;
                case "parcel": if (adrObject.parcel == false) {
                    adrObject.parcel = true;
                    cdbRunner.AddressDisplay("parcel", cardObject.value);
                  } else { vCardExtras = cdbRunner.LineAppend(vCardExtras, tempText); }
                break;
                case "home": if (adrObject.home == false) {
                    adrObject.home = true;
                    cdbRunner.AddressDisplay("home", cardObject.value);
                  } else { vCardExtras = cdbRunner.LineAppend(vCardExtras, tempText); }
                break;
                case "work": if (adrObject.work == false) {
                    adrObject.work = true;
                    cdbRunner.AddressDisplay("work", cardObject.value);
                  } else { vCardExtras = cdbRunner.LineAppend(vCardExtras, tempText); }
                break;
              }
            }
          break;
          case "LABEL": tempText2 = String(cardObject.params).toLowerCase();
            while (tempText2.indexOf("type=") >= 0) { tempText2 = tempText2.replace("type=",""); }
            tempArray1 = tempText2.split(",");
            localSplitIdx = tempArray1.indexOf("pref");
            if (localSplitIdx >= 0) { tempArray1.splice(localSplitIdx,1); }
            for (localSplitIdx = 0; localSplitIdx < tempArray1.length; localSplitIdx++) {
              tempText3 = tempArray1[localSplitIdx];
              if (tempText3.indexOf("language=") >= 0) { tempArray1.splice(localSplitIdx,1); }
            }
            if (tempArray1.length == 0) {
              tempArray1.push("intl");
              tempArray1.push("postal");
              tempArray1.push("parcel");
              tempArray1.push("work");
            }
            tempText2 = cdbRunner.UnescapeChars(cardObject.value);
            document.getElementById("divLABEL").hidden = false;
            for (localSplitIdx = 0; localSplitIdx < tempArray1.length; localSplitIdx++) {
              switch (tempArray1[localSplitIdx]) {
                case "dom": if (lblObject.dom == false) {
                    lblObject.dom = true;
                    document.getElementById("divLABELdom").hidden = false;
                    document.getElementById("vcardLBLdom").innerText = tempText2;
                  } else { vCardExtras = cdbRunner.LineAppend(vCardExtras, tempText); }
                break;
                case "intl": if (lblObject.intl == false) {
                    lblObject.intl = true;
                    document.getElementById("divLABELintl").hidden = false;
                    document.getElementById("vcardLBLintl").innerText = tempText2;
                  } else { vCardExtras = cdbRunner.LineAppend(vCardExtras, tempText); }
                break;
                case "postal": if (lblObject.postal == false) {
                    lblObject.postal = true;
                    document.getElementById("divLABELpostal").hidden = false;
                    document.getElementById("vcardLBLpostal").innerText = tempText2;
                  } else { vCardExtras = cdbRunner.LineAppend(vCardExtras, tempText); }
                break;
                case "parcel": if (lblObject.parcel == false) {
                    lblObject.parcel = true;
                    document.getElementById("divLABELparcel").hidden = false;
                    document.getElementById("vcardLBLparcel").innerText = tempText2;
                  } else { vCardExtras = cdbRunner.LineAppend(vCardExtras, tempText); }
                break;
                case "home": if (lblObject.home == false) {
                    lblObject.home = true;
                    document.getElementById("divLABELhome").hidden = false;
                    document.getElementById("vcardLBLhome").innerText = tempText2;
                  } else { vCardExtras = cdbRunner.LineAppend(vCardExtras, tempText); }
                break;
                case "work": if (lblObject.work == false) {
                    lblObject.work = true;
                    document.getElementById("divLABELwork").hidden = false;
                    document.getElementById("vcardLBLwork").innerText = tempText2;
                  } else { vCardExtras = cdbRunner.LineAppend(vCardExtras, tempText); }
                break;
              }
            }
          break;
          case "VERSION":
          case "BEGIN":
          case "END": tempText2 = "";
          break;
          default: vCardExtras = cdbRunner.LineAppend(vCardExtras, tempText);
          break;
        }
      }
    }
    if (vCardExtras.length > 0) {
      document.getElementById("divOTHER").hidden = false;
      document.getElementById("vcardOTHER").innerText = vCardExtras;
    }
    var tempLI = null;
    var tempA = null;
    if (emailArray.length > 0) {
      document.getElementById("divEMAIL").hidden = false;
      tempText = "";
      for (localArrayIdx = 0; localArrayIdx < emailArray.length; localArrayIdx++) {
        tempText1 = "";
        tempText2 = "";
        tempArray1 = String(emailArray[localArrayIdx]).split(":");
        if (tempArray1.length > 1) {
          tempText1 = tempArray1[0];
          tempArray1.splice(0,1);
          tempText2 = tempArray1.join(":");
        } else { tempText2 = tempArray1[0]; }
        tempLI = document.createElement("li");
        tempLI.innerText = tempText1+" ";
        tempA = document.createElement("a");
        tempA.setAttribute("href","mailto:"+tempText2);
        tempA.innerText = tempText2;
        tempLI.appendChild(tempA);
        document.getElementById("vcardEMAIL").appendChild(tempLI);
      }
    }
    if (telArray.length > 0) {
      document.getElementById("divTEL").hidden = false;
      tempText = "";
      for (localArrayIdx = 0; localArrayIdx < telArray.length; localArrayIdx++) {
        tempText1 = "";
        tempText2 = "";
        tempArray1 = String(telArray[localArrayIdx]).split(":");
        if (tempArray1.length > 1) {
          tempText1 = cdbRunner.UpperCaseInitialLetter(tempArray1[0]);
          tempArray1.splice(0,1);
          tempText2 = tempArray1.join(":");
        } else { tempText2 = tempArray1[0]; }
        tempLI = document.createElement("li");
        tempLI.innerText = tempText1+" ";
        tempA = document.createElement("a");
        tempA.setAttribute("href","tel:"+tempText2);
        tempA.innerText = tempText2;
        tempLI.appendChild(tempA);
        document.getElementById("vcardTEL").appendChild(tempLI);
      }
    }
  },

  cdbContactDisplayStage2: function(channelData) {
    var localCharset = String(channelData.getResponseHeader("Content-Type")).toLowerCase();
    var localStatus = channelData.status;
    if ((localCharset.indexOf("utf-8",0) >= 0) &&
        ((localStatus == 200) || (localStatus == 206) || (localStatus == 207))) {
      // MUST have something to process so only accept 200:OK, 206:Partial Content and 207:Multi-Status
      var responseElements = channelData.responseXML.documentElement.getElementsByTagNameNS("DAV:","response");
      if (responseElements.length > 0) { // Only bother doing something if there are entries to process
        var currItem = responseElements[0];
        var itemCard = currItem.getElementsByTagNameNS("urn:ietf:params:xml:ns:carddav","address-data")[0].textContent;
        cdbRunner.cdbContactDisplayStage3(itemCard);
      } else {
        document.getElementById("cdbstatus").innerText = browser.i18n.getMessage("cdbCardLoadPromptMsg2");
      }
    } else {
      document.getElementById("cdbstatus").innerText = browser.i18n.getMessage("cdbCardLoadPromptMsg1");
    }
  },

  cdbContactDisplayStage1: function() {
    const vcardHREF = document.getElementById("vcardname").getAttribute("vcard-href");
    var vcardQuery = "<?xml version=\"1.0\" encoding=\"utf-8\" ?>\n"+
                     "<C:addressbook-multiget xmlns:D=\"DAV:\" xmlns:C=\"urn:ietf:params:xml:ns:carddav\">\n"+
                     "  <D:prop>\n    <C:address-data content-type=\"text/vcard\" version=\"3.0\" />\n  </D:prop>\n";
    vcardQuery = vcardQuery + "  <D:href>" + vcardHREF + "</D:href>\n</C:addressbook-multiget>\n";
    var vcardQueryStage2Channel = new XMLHttpRequest;
    vcardQueryStage2Channel.addEventListener("load",function(e) { cdbRunner.cdbContactDisplayStage2(vcardQueryStage2Channel); },false);
    vcardQueryStage2Channel.addEventListener("error",function(e) { cdbRunner.cdbSecurityInformation(vcardQueryStage2Channel); },false);
    vcardQueryStage2Channel.addEventListener("timeout",function(e) { cdbRunner.cdbTimeOut(); },false);
    vcardQueryStage2Channel.addEventListener("progress",function(e) { cdbRunner.cdbNetProgress(vcardQueryStage2Channel); },false);
    vcardQueryStage2Channel.open("REPORT",cdbRunner.cdbURLpath,true);
    vcardQueryStage2Channel.timeout = 60000; // Timeout = 60 seconds
    vcardQueryStage2Channel.setRequestHeader("Depth","1");
    vcardQueryStage2Channel.send(vcardQuery);
  },

  LoadContacts: function(channelData) {
    var localCharset = String(channelData.getResponseHeader("Content-Type")).toLowerCase();
    var localStatus = channelData.status;
    var tempText = "";
    var tempText1 = "";
    if ((localCharset.indexOf("utf-8",0) >= 0) &&
        ((localStatus == 200) || (localStatus == 206) || (localStatus == 207))) {
      // MUST have something to process so only accept 200:OK, 206:Partial Content and 207:Multi-Status
      // Initial processing: Just pull the vCard text, FN and N elements.
      var responseElements = channelData.responseXML.documentElement.getElementsByTagNameNS("DAV:","response");
      if (responseElements.length > 0) { // Only bother doing something if there are entries to process
        document.getElementById("cdbstatus").innerText = browser.i18n.getMessage("cdbURLstatus8");
        var tableContent = document.getElementById("contactsbody");
        var currItem = null;
        var itemCard = "";
        var itemHREF = "";
        var itemStatus = "";
        var tempText = "";
        var localCounter = 0;
        var localCounter1 = 0;
        var tempIdx = 0;
        var tempArray1 = null;
        var tempArray2 = null;
        var cardObject = null;
        var tableRow = null;
        var tableCol = null;
        var localFullname = "";
        var localFirstname = "";
        var localLastname = "";
        // Extract and process vCards
        for (localCounter = 0; localCounter < responseElements.length; localCounter++) {
          currItem = responseElements[localCounter];
          itemStatus = currItem.getElementsByTagNameNS("DAV:","status")[0].textContent;
          if (String(itemStatus).indexOf("200") >= 0) {
            itemCard = currItem.getElementsByTagNameNS("urn:ietf:params:xml:ns:carddav","address-data")[0].textContent;
            itemHREF = String(currItem.getElementsByTagNameNS("DAV:","href")[0].textContent); // HREF
            // Process the vCards
            itemCard = cdbRunner.EnforceLineEndgings(itemCard);
            currItem = cdbRunner.Unfold(itemCard);
            tempArray1 = currItem.split(String.fromCharCode(13, 10));
            localFullname = "";
            localFirstname = "";
            localLastname = "";
            for (localCounter1 = 0; localCounter1 < tempArray1.length; localCounter1++) {
              tempText = String(tempArray1[localCounter1]).trim();
              cardObject = cdbRunner.LineSplit(tempText);
              tempText = String(cardObject.tag).toUpperCase();
              tempIdx = tempText.indexOf(".");
              if (tempIdx >= 0) { tempText = tempText.substring((tempIdx+1),undefined); }
              if (tempText == "FN") {
                localFullname = cdbRunner.UnescapeChars(cardObject.value);
              } else if (tempText == "N") {
                tempArray2 = cdbRunner.SafeSplitOnChar(cardObject.value,";");
                switch (tempArray2.length) {
                case 0: localFirstname = ""; localLastname = "";
                  break;
                case 1: localFirstname = ""; localLastname = cdbRunner.UnescapeChars(tempArray2[0]);
                  break;
                default: localFirstname = cdbRunner.UnescapeChars(tempArray2[1]);
                         localLastname = cdbRunner.UnescapeChars(tempArray2[0]);
                  break;
                }
              }
            }
            tableRow = document.createElement("tr");
            tableRow.setAttribute("id", "vcard"+String(localCounter));
            tableRow.setAttribute("vcard-href", itemHREF);
            tableCol = document.createElement("td"); tableCol.innerText = localFullname;
            tableCol.setAttribute("id", "vcard"+String(localCounter)+"td");
            tableRow.appendChild(tableCol);
            tableCol = document.createElement("td"); tableCol.innerText = localFirstname;
            tableRow.appendChild(tableCol);
            tableCol = document.createElement("td"); tableCol.innerText = localLastname;
            tableRow.appendChild(tableCol);
            tableContent.appendChild(tableRow);
          }
        }
        document.getElementById("cdbstatus").innerText = browser.i18n.getMessage("cdbURLloadLabel1");
      } else {
        document.getElementById("cdbstatus").innerText = browser.i18n.getMessage("cdbURLstatus7");
        cdbRunner.cdbURLdisconnect();
      }
    } else {
      document.getElementById("cdbstatus").innerText = browser.i18n.getMessage("cdbURLstatus6");
      cdbRunner.cdbURLdisconnect();
    }
  },

  cdbQueryForCards: function(channelData) {
    // Check for and extract the HREF entries for the CardDAV contacts
    var localCharset = String(channelData.getResponseHeader("Content-Type")).toLowerCase();
    var localStatus = channelData.status;
    var tempText = "";
    var tempText1 = "";
    if ((localCharset.indexOf("utf-8",0) >= 0) &&
        ((localStatus == 200) || (localStatus == 206) || (localStatus == 207))) {
      var responseElements = channelData.responseXML.documentElement.getElementsByTagNameNS("DAV:","response");
      var pathLength = String(cdbRunner.cdbURLpath).length;
      if (responseElements.length > 0) { // Only bother doing something if there are entries to process
        var hrefArray = new Array();
        var tempIdx = 0;
        var currItem = null;
        for (tempIdx = 0; tempIdx < responseElements.length; tempIdx++) {
          currItem = responseElements[tempIdx];
          tempText = String(currItem.getElementsByTagNameNS("DAV:","href")[0].textContent); // HREF
          tempText1 = String(currItem.getElementsByTagNameNS("DAV:","status")[0].textContent); // Item Status
          if ((tempText1.indexOf("200") >= 0) && (tempText.length > pathLength)) { hrefArray.push(tempText); }
        }
        if (hrefArray.length > 0) {
          // Build the query body
          tempText = "<?xml version=\"1.0\" encoding=\"utf-8\" ?>\n"+
                     "<C:addressbook-multiget xmlns:D=\"DAV:\" xmlns:C=\"urn:ietf:params:xml:ns:carddav\">\n"+
                     "  <D:prop>\n    <C:address-data content-type=\"text/vcard\" version=\"3.0\" />\n  </D:prop>\n";
          for (tempIdx = 0; tempIdx < hrefArray.length; tempIdx++) {
            tempText1 = hrefArray[tempIdx];
            tempText = tempText + "  <D:href>" + tempText1 + "</D:href>\n";
          }
          tempText = tempText + "</C:addressbook-multiget>\n";
          document.getElementById("cdbstatus").innerText = browser.i18n.getMessage("cdbURLstatus5");
          var vCardChannel = new XMLHttpRequest;
          vCardChannel.addEventListener("load",function(e) { cdbRunner.LoadContacts(vCardChannel); },false);
          vCardChannel.addEventListener("error",function(e) { cdbRunner.cdbSecurityInformation(vCardChannel); },false);
          vCardChannel.addEventListener("timeout",function(e) { cdbRunner.cdbTimeOut(); },false);
          vCardChannel.addEventListener("progress",function(e) { cdbRunner.cdbNetProgress(vCardChannel); },false);
          vCardChannel.open("REPORT",cdbRunner.cdbURLpath,true);
          vCardChannel.timeout = 60000; // Timeout = 60 seconds
          vCardChannel.setRequestHeader("Depth","1");
          vCardChannel.send(tempText);
        } else {
          document.getElementById("cdbstatus").innerText = browser.i18n.getMessage("cdbURLstatus4");
          cdbRunner.cdbURLdisconnect();
        }
      } else {
        document.getElementById("cdbstatus").innerText = browser.i18n.getMessage("cdbURLstatus4");
        cdbRunner.cdbURLdisconnect();
      }
    } else {
      document.getElementById("cdbstatus").innerText = browser.i18n.getMessage("cdbURLstatus4");
      cdbRunner.cdbURLdisconnect();
    }
  },

  cdbCheckDAVsupport: function(channelData) {
    // Check the server for CardDAV support and then query for vCard HREF entries
    var localCardDAVcheck = String(channelData.getResponseHeader("DAV")).indexOf("addressbook",0);
    if (localCardDAVcheck >= 0) {
      var stage1Query = "<?xml version=\"1.0\" encoding=\"utf-8\" ?>\n"+
                        "<D:propfind xmlns:D=\"DAV:\">\n  <D:prop>\n"+
                        "    <D:getetag />\n  </D:prop>\n</D:propfind>";
      var elementRef = document.getElementById("URLinput"); elementRef.disabled = true;
      elementRef = document.getElementById("URLconnect"); elementRef.disabled = true;
      elementRef = document.getElementById("URLdisconnect"); elementRef.disabled = false;
      document.getElementById("cdbstatus").innerText = browser.i18n.getMessage("cdbURLstatus2");
      var DAVchannel = new XMLHttpRequest;
      DAVchannel.addEventListener("load",function(e) { cdbRunner.cdbQueryForCards(DAVchannel); },false);
      DAVchannel.addEventListener("error",function(e) { cdbRunner.cdbSecurityInformation(DAVchannel); },false);
      DAVchannel.addEventListener("timeout",function(e) { cdbRunner.cdbTimeOut(); },false);
      DAVchannel.addEventListener("progress",function(e) { cdbRunner.cdbNetProgress(DAVchannel); },false);
      DAVchannel.open("PROPFIND",cdbRunner.cdbURLpath,true);
      DAVchannel.timeout = 60000; // Timeout = 60 seconds
      DAVchannel.setRequestHeader("Depth","1");
      DAVchannel.send(stage1Query);
    } else {
      document.getElementById("cdbstatus").innerText = browser.i18n.getMessage("cdbURLstatus3");
    }
  },

  cdbURLconnect: function() {
    // Check that URL if properly formatted
    cdbRunner.cdbURLpath = String(document.getElementById("URLinput").value);
    const URLPattern = /^(([^:\/?#]+):)?(\/\/([^\/?#]*))?([^?#]*)(\?([^#]*))?(#(.*))?/i;
    let localURIdecode = URLPattern[Symbol.match](cdbRunner.cdbURLpath);
    // From RFC-3986 - URI - Generic Syntax
    // Excample == http://www.ics.uci.edu/pub/ietf/uri/#Related
    // $1 == http:                  $2 == http
    // $3 == //www.ics.uci.edu      $4 == www.ics.uci.edu
    // $5 == /pub/ietf/uri/         $6 == <undefined>
    // $7 == <undefined>            $8 == #Related
    // $9 == Related
    var localProtocol = localURIdecode[2].toUpperCase();
    if ((localProtocol == 'HTTP') || (localProtocol == 'HTTPS')) {
      try {
        document.getElementById("cdbstatus").innerText = browser.i18n.getMessage("cdbURLstatus1");
        var LocalChannel = new XMLHttpRequest;
        LocalChannel.addEventListener("load",function(e) { cdbRunner.cdbCheckDAVsupport(LocalChannel); },false);
        LocalChannel.addEventListener("error",function(e) { cdbRunner.cdbSecurityInformation(LocalChannel); },false);
        LocalChannel.addEventListener("timeout",function(e) { cdbRunner.cdbTimeOut(); },false);
        LocalChannel.addEventListener("progress",function(e) { cdbRunner.cdbNetProgress(LocalChannel); },false);
        LocalChannel.open("OPTIONS",cdbRunner.cdbURLpath,true);
        LocalChannel.timeout = 60000; // Timeout = 60 seconds
        LocalChannel.send(null);
      } catch (e) {
        document.getElementById("cdbstatus").innerText = browser.i18n.getMessage("cdbURLconnectPromptMessage2");
      }
    } else {
      document.getElementById("cdbstatus").innerText = browser.i18n.getMessage("cdbURLconnectPromptMessage1");
    }
  }
}

document.addEventListener("DOMContentLoaded", function () { cdbRunner.onLoad(); }, false);
