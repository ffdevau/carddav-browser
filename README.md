# CardDAV Browser



## Description
CardDAV Browser gives you a quick and easy way to connect to a CardDAV server and view vCard details.

## Installation
Install directly from the [CardDAV Browser](https://addons.mozilla.org/en-US/firefox/addon/carddav-browser/) Extension page.

## License
[Mozilla Public License, version 2.0](http://www.mozilla.org/MPL/2.0/)

## Screenshots

CardDAV Browser Taskbar Icon

![CardDAV Browser Taskbar Icon](screenshots/cdb-01.png)


CardDAV Browser Empty URL Dialog

![CardDAV Browser Empty URL Dialog](screenshots/cdb-02.png)


CardDAV Browser URL Dialog With Address

![CardDAV Browser URL Dialog With Address](screenshots/cdb-03.png)


CardDAV Browser Authentication Prompt

![CardDAV Browser Authentication Prompt](screenshots/cdb-04.png)


CardDAV Browser With vCards Loaded

![CardDAV Browser With vCards Loaded](screenshots/cdb-05.png)


CardDAV Browser With Entry Selected

![CardDAV Browser With Entry Selected](screenshots/cdb-06.png)


vCard Details - Part 1

![vCard Details - Part 1](screenshots/cdb-07.png)


vCard Details - Part 2

![vCard Details - Part 2](screenshots/cdb-08.png)
